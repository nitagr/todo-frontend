import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import swal from 'sweetalert';
import { SignupBody } from '../API';
import { postUserSignupData } from '../services/FetchNodeServices';
import Header from './Header';

// styles for Sign up form
const useStyles = makeStyles({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
    padding: 10,
  },
  subdiv: {
    width: 350,
    boxShadow: '0 5px 8px 0 rgba(0,0,0,0.2), 0 7px 20px 0 rgba(0,0,0,0.17)',
    height: 230,
    padding: 10,
    borderRadius: 10
  },
});

const  Signup = () => {
  const classes = useStyles();
  const history = useHistory();
  
  // states for name, email, password field used in sign up
  const [name, setName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  
  // handler for signup, which takes required fields for sign up 
  // and makes a request at `users/signup` 
  const handleSignup = async () => {
    const body: SignupBody = {
      name: name,
      email: email,
      password: password,
    };

    // calling backend API using Node services
    const response = await postUserSignupData(
      'users/signup',
      body
    );

    if(response?.status === 201) {
      swal({
        title: "Signup Successful",
        icon: "success",
        dangerMode: false,
      });
      history.push('/user/signin');
    } else {
        swal({
          title: "Signup Failed",
          icon: "warning",
          dangerMode: true,
        });
    }
  }
  
  return (
    <>
      <Header loggedUser={false} />
      <div className={classes.root}>
        <div className={classes.subdiv}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={12}>
              <TextField
                label="Name"
                fullWidth
                variant="outlined"
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
              />
            </Grid>

            <Grid item xs={12} sm={12}>
              <TextField
                label="Email"
                fullWidth
                variant="outlined"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </Grid>

            <Grid item xs={12} sm={12}>
              <TextField
                label="Password"
                fullWidth
                variant="outlined"
                value={password}
                type="password"
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </Grid>
            <Grid item xs={12} sm={4}></Grid>
            <Grid item xs={12} sm={4} justify="center">
              <Button
                variant="contained"
                fullWidth
                style={{
                  backgroundColor: "#fbf343",
                }}
                onClick={handleSignup}
              >Sign Up
              </Button>
            </Grid>
            <Grid item xs={12} sm={4}></Grid>
          </Grid>
        </div>
      </div>
    </>
  );
}

export default Signup;
