// Body for saving todo data in post request
export interface SaveTodoBody {
  title: string,
  description: string,
}

// Todo interface for incoming Todo data on get request through server
export interface Todo {
  _id: string,
  title: string,
  description: string,
}

// Body for user signup post request
export interface SignupBody {
  name: string,
  email: string,
  password: string,
}

// Body for user signin post request
export interface SigninBody {
  email: string,
  password: string,
}

// Login Info for passing data through history to another path
export interface LoginInfo {
  pathname: string,
  data: {
    expiresIn: string,
    token: string,
    email: string
  }   
}

// Todo state for reducer
export type TodoState = {
  todos: Todo[]
}

// Todo action for reducer
export type TodoAction = {
  type: string
  payload: Todo
}
