import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';

import './App.css';
import AddTodo from './components/AddTodo';
import Signup from './components/Signup';
import Signin from './components/Signin';
import Header from './components/Header';

function App() {

  return (
    <Router>
      <div >
        <Route exact path="/">
          <Header loggedUser={false} />
        </Route>
        <Route path="/user/signup">
          <Signup />
        </Route>
        <Route path="/user/signin">
          <Signin />
        </Route>
        <Route path="/todo/add">
          <AddTodo />
        </Route>
      </div>
    </Router>
  );
}

export default App;
