import { SaveTodoBody, SignupBody, SigninBody, Todo } from '../API';
import 'dotenv/config';

export const SERVER_URL = process.env.SERVER_URL || 'http://localhost:5000';
const errorData: Todo[] = [];

// service to post only data to backend server
export const postTodoData = async (URL: string, body: SaveTodoBody) => {
  try {
    const response = await fetch(`${SERVER_URL}/${URL}`, {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      headers: { 
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: JSON.stringify(body),
    });
    const result = await response.json();
    return result;
  } catch (err) {
      return null;
    }
};

// service to fetch data from backend server
export const getTodoData = async (URL: string): Promise<Todo[]> => {
  try {
    let response = await fetch(`${SERVER_URL}/${URL}`, {
      method: 'GET',
      mode: 'cors',
      credentials: 'include',
      headers: { 
        'Content-Type': 'application/json;charset=utf-8',
      }
    });
    let result = await response.json();
    return result.data;
  } catch (err) {
      return errorData;
  }
}

// service to delete todo by its id
export const deleteTodoData = async (URL: string) => {
  try {
    const response = await fetch(`${SERVER_URL}/${URL}`, {
      method: "DELETE",
      mode: "cors",
      credentials: 'include',
      headers: { 
        'Content-Type': 'application/json;charset=utf-8',
      }
    });
    const result = await response.json();
    return result;
  } catch (e) {
      return null;
  }
};

// service to delete todo by its id
export const deleteAllTodoData = async (URL: string) => {
  try {
    const response = await fetch(`${SERVER_URL}/${URL}`, {
      method: "DELETE",
      mode: "cors",
      credentials: 'include',
      headers: { 
        'Content-Type': 'application/json;charset=utf-8',
      }
    });
    const result = await response.json();
    return result;
  } catch (e) {
      return null;
  }
};


// service to post only data to backend server
export const postUserSignupData = async (URL: string, body: SignupBody) => {
  try {
    const response = await fetch(`${SERVER_URL}/${URL}`, {
      method: "POST",
      mode: "cors",
      credentials: 'include',
      headers: { "Content-Type": "application/json;charset=utf-8" },
      body: JSON.stringify(body),
    });
    const result = await response.json();
    return result;
  } catch (err) {
      return null;
    }
};

// service to post only data to backend server
export const postUserSigninData = async (URL: string, body: SigninBody) => {
  try {
    const response = await fetch(`${SERVER_URL}/${URL}`, {
      method: "POST",
      mode: "cors",
      credentials: 'include',
      headers: { "Content-Type": "application/json;charset=utf-8" },
      body: JSON.stringify(body),
    });
    const result = await response.json();
    return result;
  } catch (err) {
      return null;
    }
};

// service for logout the loggedin user
export const postLogout = async (URL: string) => {
  try {
    const response = await fetch(`${SERVER_URL}/${URL}`, {
      method: "POST",
      mode: "cors",
      credentials: 'include',
      headers: { "Content-Type": "application/json;charset=utf-8" },
    });
    const result = await response.json();
    return result;
  } catch (err) {
      return null;
    }
};